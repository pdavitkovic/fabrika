﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fabrika.Resources
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Colors : ResourceDictionary
    {
        public Colors()
        {
            InitializeComponent();
        }
    }
}