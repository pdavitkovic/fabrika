﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fabrika.Resources
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Fonts : ResourceDictionary
    {
        public Fonts()
        {
            InitializeComponent();
        }
    }
}