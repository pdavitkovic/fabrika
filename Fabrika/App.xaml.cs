﻿using Fabrika.PageModels;
using FreshMvvm;
using Xamarin.Forms;

namespace Fabrika
{
    public partial class App : Application
    {
        public App()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTg5MTA5QDMxMzcyZTM0MmUzMGpMZmI4T2pqdGRJUWNycDZaWHBvcVhhU2U3MWlZUytOa1hHdGFtZFNhZzQ9");

            InitializeComponent();

            var page = FreshPageModelResolver.ResolvePageModel<LoginPageModel>();
            var basicNavContainer = new FreshNavigationContainer(page);
            MainPage = basicNavContainer;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
