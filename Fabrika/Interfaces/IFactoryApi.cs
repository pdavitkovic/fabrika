﻿using System.Threading.Tasks;
using Refit;
using Fabrika.Models;
using Fabrika.Models.Request;
using System.Collections.Generic;

namespace Fabrika.Interfaces
{
    [Headers("Content-type: application/json")]
    public interface IFactoryApi
    {
        [Post("/api/login.json")]
        Task<User> Login([Body] Login login);

        [Delete("/api/logout.json")]
        Task<User> Logout([Header("Authorization")] string token); //token treba za sve nadole

        //temp for test
        [Get("/api/events")]
        Task<User> GetEvents([Header("Authorization")] string token);

        [Get("/api/events")]
        Task<List<Event>> GetEventsGoodOne([Header("Authorization")] string token);

        [Get("/api/event_types")]
        Task<List<EventType>> GetEventTypes([Header("Authorization")] string token);

        [Put("/api/events/{id}/reservation.json")]
        Task<Reservation> MakeReservation(int id, [Header("Authorization")] string token);

        [Delete("/api/events/{id}/reservation.json")]
        Task<Cancellation> RemoveReservation(int id, [Header("Authorization")] string token);
    }
}
