﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fabrika.Models
{
    public class User
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("membership_updated_at")]
        public DateTimeOffset MembershipUpdatedAt { get; set; }

        [JsonProperty("subscribed_event_types")]
        public List<string> SubscribedEventTypes { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
