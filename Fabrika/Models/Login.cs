﻿using Newtonsoft.Json;

namespace Fabrika.Models.Request
{
    public class Login
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
