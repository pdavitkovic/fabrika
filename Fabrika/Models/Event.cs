﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fabrika.Models
{
    public class Event
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("start_date")]
        public DateTimeOffset StartDate { get; set; }

        [JsonProperty("end_date")]
        public DateTimeOffset EndDate { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("rec_type")]
        public string RecType { get; set; }

        [JsonProperty("event_type")]
        public string EventType { get; set; }

        [JsonProperty("max_users")]
        public long MaxUsers { get; set; }

        [JsonProperty("event_length")]
        public object EventLength { get; set; }

        [JsonProperty("event_pid")]
        public object EventPid { get; set; }

        [JsonProperty("users")]
        public long Users { get; set; }

        [JsonProperty("reserved")]
        public bool Reserved { get; set; }

        [JsonProperty("full")]
        public bool Full { get; set; }

        [JsonProperty("past")]
        public bool Past { get; set; }

        [JsonProperty("instructor_name")]
        public string InstructorName { get; set; }

        [JsonProperty("reserved_by")]
        public List<string> ReservedBy { get; set; }

        [JsonProperty("allowed")]
        public bool Allowed { get; set; }
    }
}
