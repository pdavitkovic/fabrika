﻿using FreshMvvm;

namespace Fabrika.PageModels
{
    public class LoginPageModel : FreshBasePageModel
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public FreshAwaitCommand LoginCommand => new FreshAwaitCommand(async (item, tcs) =>
        {
        });
        //async Task CallApi()
        //{
        //    var FactoryAPI = RestService.For<IFactoryApi>("http://factorynis.com");

        //    //moze ovako u var
        //    var events = await FactoryAPI.GetEvents();

        //    //moze i sa POCO klasom
        //    User testUser = new User();
        //    testUser = await FactoryAPI.GetEvents();
        //}
    }
}
