﻿using FreshMvvm;
using Fabrika.Custom.Popup;

namespace Fabrika.PageModels
{
    public class SchedulerPageModel : FreshBasePageModel
    {
        public FreshAwaitCommand OpenPopupCommand => new FreshAwaitCommand(async (item, tcs) =>
        {
            await CoreMethods.PushPopupPageModel<TrainingPopupPageModel>();
            tcs.SetResult(true);
        });

        public FreshAwaitCommand OpenAddAppoitmentPopupCommand => new FreshAwaitCommand(async (item, tcs) =>
        {
            await CoreMethods.PushPopupPageModel<TrainingPopupAddedSlotPageModel>();
            tcs.SetResult(true);
        });

        public FreshAwaitCommand OpenCancelPopupCommand => new FreshAwaitCommand(async (item, tcs) =>
        {
            await CoreMethods.PushPopupPageModel<TrainingCancelPopupPageModel>();
            tcs.SetResult(true);
        });
    }
}