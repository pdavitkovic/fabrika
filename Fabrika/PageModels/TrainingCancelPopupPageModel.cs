﻿using Fabrika.Custom.Popup;
using FreshMvvm;

namespace Fabrika.PageModels
{
    public class TrainingCancelPopupPageModel : FreshBasePageModel
    {
        public FreshAwaitCommand ClosePopupCommand => new FreshAwaitCommand(async (tcs) =>
        {
            await CoreMethods.PopPopupPageModel();
            await CoreMethods.PopToRoot(false);
            tcs.SetResult(true);
        });
    }
}
