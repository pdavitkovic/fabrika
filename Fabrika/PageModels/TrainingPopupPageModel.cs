﻿using Fabrika.Custom.Popup;
using FreshMvvm;
using Syncfusion.SfSchedule.XForms;
using System;

namespace Fabrika.PageModels
{
    public class TrainingPopupPageModel: FreshBasePageModel
    {
        public FreshAwaitCommand ClosePopupCommand => new FreshAwaitCommand(async (tcs) =>
        {
            await CoreMethods.PopPopupPageModel();
            await CoreMethods.PopToRoot(false);
            tcs.SetResult(true);
        });
    }
}
