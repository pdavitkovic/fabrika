﻿using Rg.Plugins.Popup.Pages;
using Xamarin.Forms.Xaml;

namespace Fabrika.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrainingPopupAddedSlotPage : PopupPage
    {
        public TrainingPopupAddedSlotPage()
        {
            InitializeComponent();
        }
    }
}